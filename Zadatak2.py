# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 13:43:50 2015

@author: User
"""
while True:
    try:
        x=float(input("Unesite ocjenu: "))
        break
    except ValueError:
        print("Niste unjeli ocjenu u odgovarajucem formatu!")

if (x<0 or x>1):
    print("Broj nije u odgovarajucem intervalu!")
elif(x>=0.9):
    print ("A")
elif(x>=0.8):
    print ("B")
elif(x>=0.7):
    print ("C")
elif(x>=0.6):
    print ("D")
else:
    print ("F")
        
            